using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Namespace
{
    public class ClassA
    {
        private int x;
        private int y;

        public int X
        {
            get { return x; }
            set { x = value; }
        }

        public int Y
        {
            get { return x; }  // Noncompliant: field 'y' is not used in the return value
            set { x = value; } // Noncompliant: field 'y' is not updated
        }
    }
}